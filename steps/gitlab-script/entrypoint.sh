#! /bin/bash

cat <<EOF | tee /tmp/script.go | go run /tmp/script.go

package main

import (
	"fmt"
	"encoding/json"
	"os"

	"github.com/xanzy/go-gitlab"
)

var _ = fmt.Printf

func main() {
	gl, err := gitlab.NewClient(os.Getenv("CI_JOB_TOKEN"))
	if err != nil {
		panic(err)
	}

	result, err := script(gl)
	if err != nil {
		panic(err)
	}

	resultString, err := json.Marshal(result)

	outputFileContents := "result=" + string(resultString)

	err = os.WriteFile(os.Getenv("STEP_RUNNER_OUTPUT"), []byte(outputFileContents), 0640)
	if err != nil {
		panic(err)
	}
}

func script(gl *gitlab.Client) (interface{}, error) {
$1
}
EOF
