## Step Runner for Agents

AI Agents use "tools" (aka "function calls") when they need to perform an action
or fetch data to in response to a user prompt. We would like to be able to use
`step-runner` for this purpose.

In order to do so, the LLM needs to be able to generate valid step definitions.

### Asks

- **Problem:** need to evaluate steps in an isolated environment (separate container) 
  - [ ] expose gRPC server?
  - [ ] streaming results -- want to show progress to user in realtime
- **Problem:** LLMs need more detailed information about how inputs and outputs are used
   so they can parameterize steps successfully
  - [ ] `spec.description` top level summary description of what the step does
  - [ ] `spec.inputs.*.description`
  - [ ] `spec.inputs.*.options` for enums
  - [ ] `spec.outputs.*` output defintions that match input definitions with same richness
- **Problem:** we need a place to describe examples of how a step can be used with other steps
- **Problem:** we need to make it easier to write "plumbing" steps that manipute I/O.
   LLMs are really good at code generation, so this provides a nice fallback for when
   there is no published step for the task
  - [ ] native scripting engine? https://github.com/risor-io/risor
  - [ ] make it easy to call GitLab APIs from scripts
  - [ ] automatically parse JSON strings in `outputs`
- **Problem:** validation error messages are not verbose/traceable enough for an LLM to
   improve generative output in a REPL
  - [ ] return all validation errors, not just the first one
  - [ ] print out the full YAML config with errors clearly indicated
     ```yaml
      - name: get_weather
        step: ./steps/weather 
        inputs:
          locations: ${{steps.find_user_location.outputs.result}}
          ^^^^^^^^^ Invalid input parameter `locations`. Did you mean `location`?
          unit: F
          ^^^^ Invalid value for input parameter `unit`. Must be one of `[fahrenheit, celsius]`
     ```
- **Problem:** performance in Agent context will be critical. What options might we have for caching downloads and invocations?
  - [ ] `step-runner` will be started in ephemeral environment, can we cache downloads without side-effects?
  - [ ] could steps be marked as idempotent and their results cached accordingly?


### Bugs

> What is the correct syntax for accessing nested steps within a repo?

- :x: `https+git://gitlab.com/josephburnett/hello-step-runner/steps/bash`
- :x: `https+git://gitlab.com/josephburnett/hello-step-runner:/steps/bash`

> Tolerant parsing of remote step URIs?

- :white_check_mark: `https+git://gitlab.com/gitlab-org/ci-cd/runner-tools/echo-step`
- :x: `https://gitlab.com/gitlab-org/ci-cd/runner-tools/echo-step`
- :x: `gitlab.com/gitlab-org/ci-cd/runner-tools/echo-step`
- :x: `gitlab-org/ci-cd/runner-tools/echo-step`
- :question: `https://gitlab.com/-/snippets/2340182` - should we support snippets for quick-and-dirty prototyping? 

### Prompt

See [./Modefile] for the system prompt used to generate these examples.
Examples were generated using Anthropic `claude-2.1`.

### Example Responses

##### What is the weather like where @marshall007 lives? Render the result using GitLab flavored markdown.

>>>
 Here is a workflow to get the weather where @marshall007 lives and render it using GitLab flavored markdown:

```yaml
steps:
- name: find_user
  step: https://gitlab.com/gitlab-org/ci-cd/runner-tools/gitlab-api
  inputs:
    script: |  
      users, _ := gl.Users.ListUsers(&gitlab.ListUserOptions{
        Username: "marshall007"
      })
      return users[0]

- name: get_location
  step: https://gitlab.com/gitlab-org/ci-cd/runner-tools/echo-step
  inputs:
    echo: ${steps.find_user.outputs.result.Location}

- name: get_weather
  step: https://gitlab.com/gitlab-org/ci-cd/runner-tools/weather 
  inputs:
    location: ${steps.get_location.outputs.echo}

- name: render_markdown 
  step: https://gitlab.com/gitlab-org/ci-cd/runner-tools/gitlab-api
  inputs:
    script: |
     output := fmt.Sprintf(`The current weather for @marshall007 is:
      
      * Temperature: %s degrees
      * Forecast High: %s degrees 
      * Forecast Low: %s degrees
      * Conditions: %s`, 
        steps.get_weather.outputs.current,
        steps.get_weather.outputs.high,
        steps.get_weather.outputs.low,
        steps.get_weather.outputs.conditions)

      res, _ := gl.Markdown.Render(&gitlab.RenderOptions{
        Text: output,
      })
      return res.HTML
```

This first finds the user @marshall007 to get their location, then gets the current weather data for that location, and finally renders it using GitLab flavored markdown. The output would show the current temperature, forecast high/low, and conditions formatted nicely.
>>>

##### Create a workflow to send an email to @marshall007 explaining how awesome this prompt is and what it does

>>>
 Here is a YAML workflow that sends an email to @marshall007 about this prompt:

```yaml
steps:
- name: find_user
  step: ./steps/gitlab-script
  inputs: 
    script: |
      users, _, err := gl.Users.ListUsers(&gitlab.ListUserOptions{
        Username: "marshall007"
      })
      return users[0], err

- name: get_email
  step: ./steps/gitlab-script
  inputs:
    script: |
      return steps.find_user.outputs.result.Email, nil

- name: render_email_body
  step: ./steps/gitlab-script
  inputs:
    script: |
     return `
       Hi Marshall,
       
       This prompt allows creating automated workflows in GitLab using YAML. It exposes some useful steps like calling the GitLab API, getting weather data etc.
       
       We can chain these steps together to do cool stuff!
       
       Have an awesome day :)
     `, nil

- name: send_email
  step: ./steps/gitlab-script
  inputs:
    script: | 
     email := steps.get_email.outputs.result
     body := steps.render_email_body.outputs.result
     
     // send email using GitLab API
     gl.NotificationSettings.NotifyViaEmail(email, "Awesome Prompt", body)
     
     return nil, nil
```

This finds Marshall's user information, grabs his email, renders an email body explaining what the prompt does, and sends it to him using the GitLab API. The workflow uses the power of the exposed steps to chain together an automated sequence to accomplish the goal.
>>>

##### Find issues in `gitlab-org/gitlab` with label `feature::ai`, print a table with the issue link and title and render it using GitLab flavored markdown

>>>
 Here is the workflow to accomplish that:

```yaml
steps:
- name: issues
  step: ./steps/gitlab-script
  inputs: 
    script: |
      issues, _, _ := gl.Issues.ListProjectIssues("gitlab-org/gitlab", &gitlab.ListProjectIssuesOptions{
        Labels: []string{"feature::ai"}
      })
      return issues

- name: format
  step: ./steps/gitlab-script
  inputs:
    script: |  
      var rows []string
      for _, issue := range steps.issues.outputs.result {
        link := fmt.Sprintf("https://gitlab.com/gitlab-org/gitlab/-/issues/%d", issue.IID)  
        row := fmt.Sprintf("| [%s](%s) | %s |", issue.Title, link, issue.Title)
        rows = append(rows, row)
      }
      table := fmt.Sprintf("# Issues\n\n| Title | Link |\n|-------|------|\n%s", strings.Join(rows, "\n"))
      return table

- name: render
  step: ./steps/gitlab-script
  inputs:
    script: |
     rendered, _, _ := gl.Markdown.Render(&gitlab.RenderOptions{
        Text: steps.format.outputs.result, 
        Project: "gitlab-org/gitlab",
     })
     return rendered.HTML
```

This workflow:

1. Fetches issues from the GitLab project with the specified label
2. Formats the issues into a Markdown table with title and link
3. Renders the Markdown table to HTML using the GitLab markdown renderer

The final output contains an HTML rendered table to display on a dashboard or docs site.
>>>