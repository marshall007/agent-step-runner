package agent

import (
	"bytes"
	"encoding/json"
	"io"
	"net/http"

	"github.com/sashabaranov/go-openai"
	"gitlab.com/marshall007/agent-step-runner/pkg/steps"
)

type AgentConfig struct {
	Name    string `json:"name,omitempty"`
	Version string `json:"version,omitempty"`

	Prompt string `json:"prompt,omitempty"`
	Model  string `json:"model,omitempty"`

	Tools []*steps.ToolExamples

	client *http.Client
}

func NewAgent(name, version, model, prompt string, tools []*steps.ToolExamples) *AgentConfig {
	ac := &AgentConfig{
		Name:    name,
		Version: version,
		Prompt:  prompt,
		Model:   model,
		Tools:   tools,
	}

	ac.client = &http.Client{}

	return ac
}

func (ac *AgentConfig) GetCompletions(system, user string) (*openai.ChatCompletionResponse, error) {
	body, err := json.Marshal(openai.ChatCompletionRequest{
		Model: ac.Model,
		Messages: []openai.ChatCompletionMessage{
			{
				Role:    "system",
				Content: system,
			},
			{
				Role:    "user",
				Content: user,
			},
		},
		MaxTokens:   3000,
		Stop:        []string{"</steps>"},
		Temperature: 0,
	})
	if err != nil {
		return nil, err
	}

	resp, err := http.Post("https://litellm-e6hvtl4yjq-uc.a.run.app/chat/completions", "application/json", bytes.NewReader(body))
	if err != nil {
		return nil, err
	}

	completion := &openai.ChatCompletionResponse{}
	data, _ := io.ReadAll(resp.Body)
	return completion, json.Unmarshal(data, &completion)
}

// func GetPipeline(data string) (*steps.Example, error) {
// 	data = strings.TrimSpace(data)
// 	data = strings.TrimLeft(data, "<pipeline>")
// 	data = strings.TrimRight(data, "</pipeline>")

// 	return steps.DecodeExample(bytes.NewBuffer(([]byte(data))))
// }
