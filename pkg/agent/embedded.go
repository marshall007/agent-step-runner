package agent

const systemPromptTemplate = `
Your goal is to create a sequential pipeline of function calls necessary to
answer the user's query. Here are the rules you must follow:

- You may invoke multiple functions, but they will be called sequentially
- You may pass outputs from one function invokation to the next by using ${...} expressions in place of $PARAMETER_VALUE that
- A "steps" context object is available within expressions. You can use it to reference outputs like so: ${steps.$STEP_NAME.outputs.$OUTPUT_NAME}
- Respond with a pipeline of function calls in <function_calls></function_calls> tags:

<function_calls>
<step>
<uri>$STEP_URI</uri>
<name>$STEP_NAME</name>
<inputs>
<$INPUT_NAME>$INPUT_VALUE</$INPUT_NAME>
...
</inputs>
</step>
</function_calls>

Here are the tools available to you:
{{ tools . }}

It is ok if you cannot answer the user's question. Do not respond with any explanation.
Respond ONLY with function definitions enclosed in <steps></steps> tags, no other text.
`

const toolsTemplate = `<tools>
{{- range .Tools }}
<step_definition>
<uri>{{ .Tool.URI }}</uri>
<description>{{ .Tool.Spec.Description }}</description>
<inputs>
{{- range $key, $input := .Tool.Spec.Inputs }}
<input>
<name>{{ $key }}</name>
<description>{{ $input.Description }}</description>
<type>{{ $input.Type }}</type>
<required>{{ $input.Required }}</required>
</input>
{{- end }}
</inputs>
<outputs>
{{- range $key, $output := .Tool.Spec.Outputs }}
<output>
<name>{{ $key }}</name>
<description>{{ $output.Description }}</description>
<type>{{ $output.Type }}</type>
</output>
{{- end }}
</outputs>
</step_definition>
{{- end }}
</tools>
`
