package agent

import (
	"bytes"
	"text/template"

	"gopkg.in/yaml.v3"
)

var toolsTpl *template.Template
var systemPromptTpl *template.Template

func init() {
	toolsTpl = template.Must(template.New("tools").Parse(toolsTemplate))
	systemPromptTpl = template.Must(template.New("system-prompt").Funcs(template.FuncMap{
		"toYaml": toYaml,
		"tools":  tools,
	}).Parse(systemPromptTemplate))
}

func (ac *AgentConfig) GetPrompt() (string, error) {
	return ac.evalTemplate(systemPromptTpl)
}

func toYaml(v interface{}) string {
	var data bytes.Buffer
	e := yaml.NewEncoder(&data)
	e.SetIndent(2)

	if err := e.Encode(&v); err != nil {
		return ""
	}
	return data.String()
}

func tools(ac *AgentConfig) (string, error) {
	return ac.evalTemplate(toolsTpl)
}

func (ac *AgentConfig) evalTemplate(t *template.Template) (string, error) {
	var out bytes.Buffer
	if err := t.Execute(&out, ac); err != nil {
		return "", err
	}

	return out.String(), nil
}
