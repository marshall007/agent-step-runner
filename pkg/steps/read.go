package steps

import (
	"errors"
	"fmt"
	"io"
	"os"
	"path"

	"gopkg.in/yaml.v3"
)

type StepArgDefinition struct {
	Type        string   `json:"type,omitempty"`
	Description string   `json:"description,omitempty"`
	Required    bool     `json:"required,omitempty"`
	Default     string   `json:"default,omitempty"`
	Options     []string `json:"options,omitempty"`
}

type StepSpec struct {
	Name        string                       `json:"name,omitempty"`
	Description string                       `json:"description,omitempty"`
	Inputs      map[string]StepArgDefinition `json:"inputs,omitempty"`
	Outputs     map[string]StepArgDefinition `json:"outputs,omitempty"`
}

type StepDefinition struct {
	Spec *StepSpec `json:"spec,omitempty"`
	URI  string    `json:"uri,omitempty"`
}

type Example struct {
	Steps []struct {
		Name   string         `json:"name,omitempty"`
		Step   string         `json:"step,omitempty"`
		Inputs map[string]any `json:"inputs,omitempty"`
	} `json:"steps,omitempty"`
}

type ToolExamples struct {
	Tool     *StepDefinition
	Examples []*Example
}

func ReadDir(dirname string) ([]*ToolExamples, error) {
	entries, err := os.ReadDir(dirname)
	if err != nil {
		return nil, fmt.Errorf("reading file: %w", err)
	}

	steps := []*ToolExamples{}

	for _, v := range entries {
		id := path.Join(dirname, v.Name())
		f, err := os.Open(path.Join(id, "step.yml"))
		if err != nil {
			return nil, fmt.Errorf("reading file: %w", err)
		}

		step, err := DecodeStep(f)
		if err != nil {
			return nil, fmt.Errorf("reading file: %w", err)
		}

		step.URI = id

		// examples, err := ReadExamples(id)
		// if err != nil {
		// 	return nil, fmt.Errorf("reading file: %w", err)
		// }

		steps = append(steps, &ToolExamples{
			Tool: step,
			// Examples: examples,
		})
	}

	return steps, nil
}

func ReadExamples(dirname string) ([]*Example, error) {
	f, err := os.Open(path.Join(dirname, "examples.yml"))
	if err != nil {
		return nil, err
	}

	examples := []*Example{}

	d := yaml.NewDecoder(f)
	for {
		spec := new(Example)
		err := d.Decode(&spec)
		// check it was parsed
		if spec == nil {
			continue
		}
		// break the loop in case of EOF
		if errors.Is(err, io.EOF) {
			break
		}
		if err != nil {
			return nil, fmt.Errorf("reading file: %s\n%w", f.Name(), err)
		}
		examples = append(examples, spec)
	}

	return examples, nil
}

func DecodeStep(r io.Reader) (*StepDefinition, error) {
	d := yaml.NewDecoder(r)

	step := new(StepDefinition)
	err := d.Decode(&step)
	if err != nil {
		return nil, err
	}

	return step, nil
}
