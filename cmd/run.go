package cmd

import (
	"encoding/json"
	"errors"
	"fmt"
	"os"

	"gitlab.com/marshall007/agent-step-runner/pkg/agent"
	"gitlab.com/marshall007/agent-step-runner/pkg/steps"

	"github.com/sashabaranov/go-openai"
	"github.com/spf13/cobra"
	"gopkg.in/yaml.v3"
)

var stepsDir string

// runCmd represents the run command
var runCmd = &cobra.Command{
	Use:   "run <config.yaml>",
	Short: "Run an AI Agent with the specified configuration",
	Args:  cobra.ExactArgs(1),
	RunE: func(cmd *cobra.Command, args []string) error {
		data, err := os.ReadFile(args[0])
		if err != nil {
			return err
		}

		var cfg agent.AgentConfig
		err = yaml.Unmarshal(data, &cfg)

		tools, err := steps.ReadDir(stepsDir)
		if err != nil {
			return err
		}

		config := agent.NewAgent(cfg.Name, cfg.Version, cfg.Model, cfg.Prompt, tools)

		// ac.Prompt, err = prompt.GetPrompt(ac.Prompt, &config.PromptTemplateData{
		// 	Agent:   ac,
		// 	Actions: actions,
		// })
		// if err != nil {
		// 	return err
		// }

		prompt, err := config.GetPrompt()
		if err != nil {
			return err
		}

		res, err := config.GetCompletions(prompt, config.Prompt)
		if err != nil {
			e := &openai.APIError{}
			if errors.As(err, &e) {
				fmt.Println("error %d: %s", e.HTTPStatusCode, e.Message)
				fmt.Println(e)
			}
			return err
		}

		// pipeline, err := prompt.GetPipeline(res.Choices[0].Message.Content)

		json.NewEncoder(os.Stdout).Encode(res)

		return err
	},
}

func init() {
	rootCmd.AddCommand(runCmd)

	runCmd.Flags().StringVarP(&stepsDir, "steps-dir", "s", "./steps", "the location of built-in step definitions")
}
