#!/bin/bash

mkdir -p "./test/output"

for filename in ./test/agents/*.yaml; do
  name=$(basename "$filename" ".yaml")
  result="test/output/$name-result.xml"

  go run ./ run "$filename" | yq -P '.choices[].message.content' > "$result"
done
